#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code.
    int a;
    long l;
    char ch;
    float f;
    double d;

    cin >> a;
    cin >> l;
    cin >> ch;
    cin >> f;
    cin >> d;

    printf("%d\n", a);
    printf("%ld\n", l);
    printf("%c\n", ch);
    printf("%f\n", f);
    printf("%lf\n", d);

    return 0;
}
