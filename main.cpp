//Mustafa Harun T�rkmeno�lu
#include <iostream>
#include <fstream>
#include <string>
#include <list>
using namespace std;

/*!
 *  \brief     Funtion Smallest.
 *  \details   Finds smallest integer in an array.
 *  \author    Mustafa Harun Turkmenoglu
 *  \version   1.0
 *  \date      2020-2021
 *  \pre       First initialize the system.
 *  \copyright Public License.
 *  @param lenght bir say�sal de�i�kendir.
 *  @param arr[] bir iteger dizidir.
 */
int Smallest(int arr[], int lenght)
{
    //if array is empty, returns 0
    if (lenght == 0)
        return 0;

    int smallest = arr[0];
    
    for (int i = 0; i < lenght; i++)
    {
        if (arr[i] < smallest)
            smallest = arr[i];
    }
    return smallest;
}

/*!
 *  \brief     Funtion Sum.
 *  \details   Finds sum of integers in an array.
 *  \author    Mustafa Harun Turkmenoglu
 *  \version   1.0
 *  \date      2020-2021
 *  \pre       First initialize the system.
 *  \copyright Public License.
 *  @param lenght bir say�sal de�i�kendir.
 *  @param sum bir say�sal de�i�kendir.
 *  @param arr[] bir iteger dizidir.
 */
int Sum(int arr[], int lenght)
{
    int sum = 0;

    for (int i = 0; i < lenght; i++)
    {
        sum = sum + arr[i];
    }
    return sum;
}

/*!
 *  \brief     Funtion Product.
 *  \details   Finds products of integers in an array.
 *  \author    Mustafa Harun Turkmenoglu
 *  \version   1.0
 *  \date      2020-2021
 *  \pre       First initialize the system.
 *  \copyright Public License.
 *  @param lenght bir say�sal de�i�kendir.
 *  @param product bir say�sal de�i�kendir.
 *  @param arr[] bir iteger dizidir.
 */
int Product(int arr[], int lenght)
{
    //if array is empty, returns 0
    if (lenght == 0)
        return 0;

    int product = 1;

    for (int i = 0; i < lenght; i++)
    {
        product = product * arr[i];
    }
    return product;
}

int main() {

    string line;
    ifstream myfile("input.txt");

    float avarage = 0.0;

    //opens the input.txt
    if (myfile.is_open())                                  
    {
        int count = 0, i = 0, noOfInts = 0;

        //gets the firs integer from txt file
        myfile >> noOfInts;                          

        //gets the firs integer from txt file
        int *arr = new int [noOfInts];  

        //assigns values to array
        while (myfile >> count)                             
        {
            arr[i] = count;                                 
            i++;
        }

        if (noOfInts == 0)
            return 0;

        if (i != noOfInts)
        {
            cout << "Error! No of integers not equals" << endl;
            myfile.close();
            return 0;
        }

        avarage = Sum(arr, noOfInts);

        //call funtions prints outputs
        cout << "Sum is " << avarage << endl;
        avarage = avarage / noOfInts;
        cout << "Product is " << Product(arr, noOfInts) << endl;
        cout << "Avarage is " << avarage << endl;
        cout << "Smallest is " << Smallest(arr, noOfInts) << endl;

    }
    else cout << "Unable to open file";

    //closes the file
    myfile.close();

    cout << endl;
    cout << endl;
    cout << endl;

    return 0;
}
